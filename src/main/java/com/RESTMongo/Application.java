package com.RESTMongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by skyshi on 25/02/16.
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) throws  Exception{

        SpringApplication.run(new Object[]{Application.class},args);

    }
}
