package com.RESTMongo.repository;

import com.RESTMongo.model.Book;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by skyshi on 25/02/16.
 */
public interface BookRepository extends MongoRepository<Book,String> {

}
